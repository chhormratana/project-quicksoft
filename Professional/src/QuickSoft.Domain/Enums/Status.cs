namespace QuickSoft.Domain.Enums
{
    public enum Status
    {
        Active,
        Disabled
    }
}