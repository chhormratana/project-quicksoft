namespace QuickSoft.Domain.Enums
{
    public enum UserType
    {
        User = 0,
        Admin = 1,
        Developer = 2,
        Subscription = 3
    }
}