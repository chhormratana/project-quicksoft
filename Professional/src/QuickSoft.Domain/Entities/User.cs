using System;
using QuickSoft.Domain.Enums;

namespace QuickSoft.Domain.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public UserType UserType { get; set; }
        
        public DateTime DateCreated { get; }  = DateTime.Now;
    }
}