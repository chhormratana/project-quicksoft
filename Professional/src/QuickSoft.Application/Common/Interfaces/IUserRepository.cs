using QuickSoft.Domain.Entities;

namespace QuickSoft.Application.Common.Interfaces
{
    public interface IUserRepository : IGenericRepository<User>
    {
        
    }
}