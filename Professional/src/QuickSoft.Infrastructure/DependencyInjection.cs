using Microsoft.Extensions.DependencyInjection;
using QuickSoft.Application.Common.Interfaces;
using QuickSoft.Infrastructure.Persistence;
using QuickSoft.Infrastructure.Repositories;

namespace QuickSoft.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            services.AddTransient<IDbConfiguration, DbConfiguration>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IUserRepository, AuditRepository>();
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            return services;
        }
    }
}