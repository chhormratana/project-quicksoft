using QuickSoft.Application.Common.Interfaces;

namespace QuickSoft.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {

        public UnitOfWork(IUserRepository userRepository)
        {
            Users = userRepository;
        }
        
        public IUserRepository Users { get; }
    }
}