using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using QuickSoft.Application.Common.Interfaces;
using QuickSoft.Domain.Entities;
using QuickSoft.Infrastructure.Persistence;

namespace QuickSoft.Infrastructure.Repositories
{
    public class AuditRepository : IUserRepository
    {
        private readonly IDbConfiguration _dbConfiguration;

        public AuditRepository(IDbConfiguration dbConfiguration)
        {
            _dbConfiguration = dbConfiguration;
        }
        
        public Task<User> Get(int id)
        {
            throw new System.NotImplementedException();
        }

        public async Task<IEnumerable<User>> GetAll()
        {
            var sql = $"SELECT * FROM Users";
            await using var connection = new SqlConnection(_dbConfiguration.GetConnectionString());
            connection.Open();
            return await connection.QueryAsync<User>(sql);
        }

        public Task<int> Add(User entity)
        {
            throw new System.NotImplementedException();
        }

        public Task<bool> Update(User entity)
        {
            throw new System.NotImplementedException();
        }

        public Task<bool> Delete(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}