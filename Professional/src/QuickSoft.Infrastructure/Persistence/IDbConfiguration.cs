namespace QuickSoft.Infrastructure.Persistence
{
    public interface IDbConfiguration
    {
        string GetConnectionString();
    }
}