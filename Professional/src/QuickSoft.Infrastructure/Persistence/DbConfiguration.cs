using Microsoft.Extensions.Configuration;

namespace QuickSoft.Infrastructure.Persistence
{
    public class DbConfiguration : IDbConfiguration
    {
        private readonly IConfiguration _configuration;

        public DbConfiguration(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public string GetConnectionString()
        {
            return _configuration.GetConnectionString("ASPNETCORE_QuickSoft_ConnectionString");
        }
    }
}