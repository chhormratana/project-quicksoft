using System;
using QuickSoft.MvcUI;
using QuickSoft.WebMvc.Settings;

namespace QuickSoft.WebMvc.ViewModels
{
    public class UserViewModel : MasterPageViewModel
    {
        public void Test()
        {
            Console.WriteLine(Context.Route?.IsSelected(RouteConfig.User));
        }
    }
}