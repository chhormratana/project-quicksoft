using System;
using System.Net;
using System.Threading.Tasks;
using QuickSoft.WebMvc.Settings;

namespace QuickSoft.WebMvc.ViewModels
{
    public class ErrorViewModel : MasterPageViewModel
    {
        public override Task PreRender()
        {
            switch (Context.Route?.RouteName)
            {
                case RouteConfig.NotFound:
                    Context.HttpContext.Response.StatusCode = (int) HttpStatusCode.NotFound;
                    break;
                case RouteConfig.InternalError:
                    Context.HttpContext.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                    break;
            }
            return base.PreRender();
        }
    }
}