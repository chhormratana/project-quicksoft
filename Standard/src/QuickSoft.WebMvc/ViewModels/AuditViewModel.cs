using System;
using QuickSoft.MvcUI;
using QuickSoft.WebMvc.Settings;

namespace QuickSoft.WebMvc.ViewModels
{
    public class AuditViewModel : MasterPageViewModel
    {
        public void Test()
        {
            Console.WriteLine(Context.Route?.IsSelected(RouteConfig.User));
        }
    }
}