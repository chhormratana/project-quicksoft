﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DotVVM.Framework.ViewModel;
using QuickSoft.MvcUI.Controls;
using QuickSoft.WebMvc.Settings;

namespace QuickSoft.WebMvc.ViewModels
{
    public class MasterPageViewModel : DotvvmViewModelBase
    {
        public NavigationDrawerHeaderItem Header { set; get; }
        public List<NavigationDrawerItem> MenuContent { set; get; }
        public AppBarItem AppBarItem { set; get; }
        public string ErrorMessage { get; set; }

        public override Task PreRender()
        {
            Header = MenuConfig.MenuHeader();
            MenuContent = MenuConfig.MenuItems(Context);
            AppBarItem = MenuConfig.AppBarItem();
            return base.PreRender();
        }
    }
}
