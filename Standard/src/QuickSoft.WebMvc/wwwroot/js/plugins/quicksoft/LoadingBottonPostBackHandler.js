dotvvm.events.init.subscribe(function () {
   dotvvm.postbackHandlers['LoadingButtonPostBackHandler'] = function (options) {
       return {
           execute: function (callback) {
               return new Promise(function (resolve, reject) {
                   let JsHandler = ko.unwrap(options.JsHandler);
                   let button = $(`#${options.buttonId}`).ladda();
                   button.ladda('start');
                   callback().then(function (resolve) {
                       button.ladda('stop');
                       let response = {
                           success: true,
                           data: options,
                           resolve: resolve
                       }
                       eval(JsHandler)(response);
                   }, function (reject) {
                       button.ladda('stop');
                       let response = {
                           success: false,
                           data: options,
                           reject: reject
                       }
                       eval(JsHandler)(response);
                   });
               });
           }
       };
   }
});