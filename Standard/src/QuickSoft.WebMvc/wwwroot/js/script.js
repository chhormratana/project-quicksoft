﻿/*
 *
 *   INSPINIA - Responsive Admin Theme
 *   version 2.9.3
 *
 */

$(document).ready(function () {

    // Fast fix bor position issue with Propper.js
    // Will be fixed in Bootstrap 4.1 - https://github.com/twbs/bootstrap/pull/24092
    Popper.Defaults.modifiers.computeStyle.gpuAcceleration = false;


    // Add body-small class if window less than 768px
    if (window.innerWidth < 769) {
        $('body').addClass('body-small')
    } else {
        $('body').removeClass('body-small')
    }

    // MetisMenu
    $('#side-menu').metisMenu();
    // Collapse ibox function
    $('.collapse-link').on('click', function (e) {
        e.preventDefault();
        let ibox = $(this).closest('div.ibox');
        let button = $(this).find('i');
        let content = ibox.children('.ibox-content');
        content.slideToggle(200);
        button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
        ibox.toggleClass('').toggleClass('border-bottom');
        setTimeout(function () {
            ibox.resize();
            ibox.find('[id^=map-]').resize();
        }, 50);
    });

    // Run menu of canvas
    $('body.canvas-menu .sidebar-collapse').slimScroll({
        height: '100%',
        railOpacity: 0.9
    });



    // Minimalize menu
    $('.navbar-minimalize').on('click', function (event) {
        event.preventDefault();
        $("body").toggleClass("mini-navbar");
        SmoothlyMenu();
        
        if (localStorageSupport()) {
            if ($('body').hasClass('mini-navbar')) {
                localStorage.setItem('collapse_menu', 'on')
            } else {
                localStorage.setItem('collapse_menu', 'off')
            }
        }
    });

    // Move right sidebar top after scroll
    $(window).scroll(function () {
        if ($(window).scrollTop() > 0 && !$('body').hasClass('fixed-nav')) {
            $('#right-sidebar').addClass('sidebar-top');
        } else {
            $('#right-sidebar').removeClass('sidebar-top');
        }
    });

    $("[data-toggle=popover]")
        .popover();

    // Add slimscroll to element
    $('.full-height-scroll').slimscroll({
        height: '100%'
    })
});

// Minimalize menu when screen is less than 768px
$(window).bind("resize", function () {
    if (window.innerWidth < 769) {
        $('body').addClass('body-small')
    } else {
        $('body').removeClass('body-small')
    }
});


// check if browser support HTML5 local storage
function localStorageSupport() {
    return (('localStorage' in window) && window['localStorage'] !== null)
}

// Local Storage functions
// Set proper body class and plugins based on user configuration
if (localStorageSupport()) {
    let body = $('body');
    let collapse = localStorage.getItem("collapse_menu");
    if (collapse === 'on') {
        if (body.hasClass('fixed-sidebar')) {
            if (!body.hasClass('body-small')) {
                body.addClass('mini-navbar');
            }
        } else {
            if (!body.hasClass('body-small')) {
                body.addClass('mini-navbar');
            }

        }
    }
}

function SmoothlyMenu() {
    let body = $('body');
    if (!body.hasClass('mini-navbar') || body.hasClass('body-small')) {
        // Hide menu in order to smoothly turn on when maximize menu
        $('#side-menu').hide();
        // For smoothly turn on menu
        setTimeout(
            function () {
                $('#side-menu').fadeIn(400);
            }, 200);
    } else if (body.hasClass('fixed-sidebar')) {
        $('#side-menu').hide();
        setTimeout(
            function () {
                $('#side-menu').fadeIn(400);
            }, 100);
    } else {
        // Remove all inline style from jquery fadeIn function to reset menu state
        $('#side-menu').removeAttr('style');
    }
}

function _ajax_request(url, data, callback, method) {
    return $.ajax({
        url: location.origin + (url.startsWith('/') ? url : ('/' + url)),
        type: method,
        data: data,
        success: callback
    });
}

$.extend({
    put: function (url, data, callback) {
        return _ajax_request(url, data, callback, 'PUT');
    }
});
$.extend({
    delete: function (url, data, callback) {
        return _ajax_request(url, data, callback, 'DELETE');
    }
});
$.extend({
    post: function (url, data, callback) {
        return _ajax_request(url, data, callback, 'POST');
    }
});
$.extend({
    get: function (url, data, callback) {
        return _ajax_request(url, data, callback, 'GET');
    }
});

$(document).ajaxStart(function() { Pace.restart(); });

