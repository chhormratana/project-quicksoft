using System.Collections.Generic;
using DotVVM.Framework.Hosting;
using QuickSoft.MvcUI;
using QuickSoft.MvcUI.Controls;

namespace QuickSoft.WebMvc.Settings
{
    public static class MenuConfig
    {
        public static NavigationDrawerHeaderItem MenuHeader()
        {
            return new()
            {
                LogoUrl = "~/img/quicksoft-technology-logo.png",
                RoundLogoUrl = "~/img/logo-round.png"
            };
        }

        public static AppBarItem AppBarItem()
        {
            return new() { Url = "#", Text = "Logout" };
        }
        
        public static List<NavigationDrawerItem> MenuItems(IDotvvmRequestContext context)
        {
            var menuItems = new List<NavigationDrawerItem>
            {
                new()
                {
                    Text = "Sale",
                    IconClass = "fal fa-shopping-cart",
                    Url = "/",
                    IsActive = context.Route.IsSelected(new [] { RouteConfig.Default , RouteConfig.Sale})
                },
                new()
                {
                    Text = "Card Package",
                    IconClass = "fal fa-address-card",
                    Url = RouteConfig.CardPackage,
                    IsActive = context.Route.IsSelected(RouteConfig.CardPackage)
                },
                new()
                {
                    Text = "Card",
                    IconClass = "fal fa-credit-card-blank",
                    Url = RouteConfig.Card,
                    IsActive = context.Route.IsSelected(RouteConfig.Card)
                },
                new()
                {
                    Text = "Customer",
                    IconClass = "fal fa-user-friends",
                    Url = RouteConfig.Customer,
                    IsActive = context.Route.IsSelected(RouteConfig.Customer)
                },
                new()
                {
                    Text = "User Account",
                    IconClass = "fal fa-user-lock",
                    Url = RouteConfig.User,
                    IsActive = context.Route.IsSelected(RouteConfig.User)
                },
                new()
                {
                    Text = "Report",
                    IconClass = "fal fa-table",
                    IsActive = context.Route.IsSelected(new [] { RouteConfig.CustomerReport, RouteConfig.PackageSaleReport}),
                    Children = new List<NavigationDrawerItem>
                    {
                        new()
                        {
                            Text = "Customer Report",
                            Url = RouteConfig.CustomerReport,
                            IsActive = context.Route.IsSelected(RouteConfig.CustomerReport)
                        },
                        new()
                        {
                            Text = "Package Sale Report",
                            Url = RouteConfig.PackageSaleReport,
                            IsActive = context.Route.IsSelected(RouteConfig.PackageSaleReport)
                        }
                    }
                }
            };

            return menuItems;
        }
    }
}