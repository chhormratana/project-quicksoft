using System;
using System.Threading.Tasks;
using DotVVM.Framework.Hosting;
using DotVVM.Framework.Runtime.Filters;
using QuickSoft.WebMvc.ViewModels;

namespace QuickSoft.WebMvc.Settings
{
    public class ErrorHandlingActionFilter : ExceptionFilterAttribute
    {
        protected override Task OnCommandExceptionAsync(IDotvvmRequestContext context, ActionInfo actionInfo, Exception exception)
        {
            // MasterPageViewModel declares the ErrorMessage property to display error messages
            // If it is set, the master page will display the error message alert.
            if (context.ViewModel is not MasterPageViewModel model)
                return base.OnCommandExceptionAsync(context, actionInfo, exception);
            model.ErrorMessage = exception.Message;
        
            Console.WriteLine("3333333333333333##########");
            Console.WriteLine(exception.Message);
                
            // We need the request to end normally, not with an error
            // context.IsCommandExceptionHandled = true;
            return base.OnCommandExceptionAsync(context, actionInfo, exception);
        }
        
        protected override Task OnPageExceptionAsync(IDotvvmRequestContext context, Exception exception)
        {
            Console.WriteLine("$$$$$$$$$$$$$$$$$$##########");
            Console.WriteLine(context.HttpContext.Response.StatusCode);
            // context.IsPageExceptionHandled = true;
            // context.RedirectToUrl($"/{RouteConfig.InternalError}");
            return base.OnPageExceptionAsync(context, exception);
        }
        
        protected override Task OnPresenterExceptionAsync(IDotvvmRequestContext context, Exception exception)
        {
            Console.WriteLine("presenter async====================");
            return base.OnPresenterExceptionAsync(context, exception);
        }
    }
}