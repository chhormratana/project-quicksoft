using DotVVM.Framework.Routing;

namespace QuickSoft.WebMvc.Settings
{
    public static class RouteConfig
    {
        public const string Default = nameof(Default);
        public const string User = nameof(User);
        public const string Audit = nameof(Audit);
        public const string NotFound = nameof(NotFound);
        public const string InternalError = nameof(InternalError);
        public const string Sale = nameof(Sale);
        public const string CardPackage = nameof(CardPackage);
        public const string Card = nameof(Card);
        public const string Customer = nameof(Customer);
        public const string CustomerReport = nameof(CustomerReport);
        public const string PackageSaleReport = nameof(PackageSaleReport);

        public static void AddRouteTable(this DotvvmRouteTable routeTable)
        {
            routeTable.Add(Default, "", "Views/Sale.dothtml");
            routeTable.Add(User, User, "Views/User.dothtml");
            routeTable.Add(Audit, Audit, "Views/Audit.dothtml");
            routeTable.Add(NotFound, NotFound, "Views/Error.dothtml");
            routeTable.Add(InternalError, InternalError, "Views/Error.dothtml");
            routeTable.Add(Sale, Sale, "Views/Sale.dothtml");
            routeTable.Add(CardPackage, CardPackage, "Views/CardPackage.dothtml");
            routeTable.Add(Card, Card, "Views/Card.dothtml");
            routeTable.Add(Customer, Customer, "Views/Customer.dothtml");
            routeTable.Add(CustomerReport, CustomerReport, "Views/CustomerReport.dothtml");
            routeTable.Add(PackageSaleReport, PackageSaleReport, "Views/PackageSaleReport.dothtml");
        }
    }
}