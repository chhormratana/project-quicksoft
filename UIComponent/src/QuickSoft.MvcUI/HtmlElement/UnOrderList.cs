using System;
using System.Linq.Expressions;
using DotVVM.Framework.Binding;
using DotVVM.Framework.Controls;
using DotVVM.Framework.Hosting;

namespace QuickSoft.MvcUI.HtmlElement
{
    public class UnOrderList : BaseElement
    {
        public UnOrderList() : base("ul")
        {
        }
    }
}