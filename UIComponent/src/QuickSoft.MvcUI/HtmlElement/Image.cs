using System;
using System.Linq.Expressions;
using DotVVM.Framework.Binding;
using DotVVM.Framework.Controls;
using DotVVM.Framework.Hosting;

namespace QuickSoft.MvcUI.HtmlElement
{
    public class Image : BaseElement
    {
        
        public static readonly DotvvmProperty AltProperty = DotvvmProperty.Register<string, Image>((Expression<Func<Image, object>>)(c => c.Alt));
        public static readonly DotvvmProperty WidthProperty = DotvvmProperty.Register<string, Image>((Expression<Func<Image, object>>)(c => c.Width));
        public static readonly DotvvmProperty SrcProperty = DotvvmProperty.Register<string, Image>((Expression<Func<Image, object>>)(c => c.Src));

        public string Alt
        {
            get => (string) GetValue(AltProperty, true);
            set
            {
                var altProperty = AltProperty;
                SetValue(altProperty, value ?? throw new ArgumentNullException(nameof (value)));
            }
        }
        
        public string Width
        {
            get => (string) GetValue(WidthProperty, true);
            set
            {
                var widthProperty = WidthProperty;
                SetValue(widthProperty, value ?? throw new ArgumentNullException(nameof (value)));
            }
        }
        
        public string Src
        {
            get => (string) GetValue(SrcProperty, true);
            set
            {
                var srcProperty = SrcProperty;
                SetValue(srcProperty, value ?? throw new ArgumentNullException(nameof (value)));
            }
        }
        public Image() : base("img")
        {
        }

        protected override void AddAttributesToRender(IHtmlWriter writer, IDotvvmRequestContext context)
        {
            if (!string.IsNullOrEmpty(Alt))
            {
                writer.AddAttribute("alt", Alt);
            }
            if (!string.IsNullOrEmpty(Width))
            {
                writer.AddAttribute("width", Width);
            }
            if (!string.IsNullOrEmpty(Src))
            {
                writer.AddAttribute("src", Src);
            }
            base.AddAttributesToRender(writer, context);
        }
    }
}