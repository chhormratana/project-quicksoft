using System;
using System.Linq.Expressions;
using DotVVM.Framework.Binding;
using DotVVM.Framework.Controls;
using DotVVM.Framework.Hosting;

namespace QuickSoft.MvcUI.HtmlElement
{
    public class Span : BaseElement
    {
        public static readonly DotvvmProperty TextProperty = DotvvmProperty.Register<string, Span>((Expression<Func<Span, object>>)(c => c.Text));


        public string Text
        {
            get => (string) GetValue(TextProperty, true);
            set
            {
                var textProperty = TextProperty;
                SetValue(textProperty, value ?? throw new ArgumentNullException(nameof (value)));
            }
        }

        public Span() : base("span")
        {
        }
        
        protected override void RenderContents(IHtmlWriter writer, IDotvvmRequestContext context)
        {
            writer.WriteUnencodedText(Text);
            base.RenderContents(writer, context);
        }
    }
}