using System;
using System.Linq.Expressions;
using DotVVM.Framework.Binding;
using DotVVM.Framework.Controls;
using DotVVM.Framework.Hosting;

namespace QuickSoft.MvcUI.HtmlElement
{
    public class Link : BaseElement
    {
        public static readonly DotvvmProperty HrefProperty = DotvvmProperty.Register<string, Link>((Expression<Func<Link, object>>)(c => c.Href));
        public static readonly DotvvmProperty TextProperty = DotvvmProperty.Register<string, Link>((Expression<Func<Link, object>>)(c => c.Text));

        public string Href
        {
            get => (string) GetValue(HrefProperty, true);
            set
            {
                var hrefProperty = HrefProperty;
                SetValue(hrefProperty, value ?? throw new ArgumentNullException(nameof (value)));
            }
        }
        
        public string Text
        {
            get => (string) GetValue(TextProperty, true);
            set
            {
                var textProperty = TextProperty;
                SetValue(textProperty, value ?? throw new ArgumentNullException(nameof (value)));
            }
        }
        
        public Link() : base("a")
        {
        }

        protected override void AddAttributesToRender(IHtmlWriter writer, IDotvvmRequestContext context)
        {
            if (!string.IsNullOrEmpty(Href))
            {
                writer.AddAttribute("href", Href);
            }
            if (!string.IsNullOrEmpty(Text))
            {
                writer.AddAttribute("text", Text);
            }
            base.AddAttributesToRender(writer, context);
        }

        protected override void RenderContents(IHtmlWriter writer, IDotvvmRequestContext context)
        {
            writer.WriteUnencodedText(Text);
            base.RenderContents(writer, context);
        }
    }
}