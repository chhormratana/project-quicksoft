using DotVVM.Framework.Controls;

namespace QuickSoft.MvcUI.HtmlElement
{
    [ControlMarkupOptions(AllowContent = false)]
    public class Icon : BaseElement
    {
        public Icon() : base("i")
        {
        }
    }
}