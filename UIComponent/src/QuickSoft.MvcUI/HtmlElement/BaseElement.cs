using System;
using System.Linq.Expressions;
using DotVVM.Framework.Binding;
using DotVVM.Framework.Controls;
using DotVVM.Framework.Hosting;

namespace QuickSoft.MvcUI.HtmlElement
{
    public abstract class BaseElement : HtmlGenericControl
    {
        public static readonly DotvvmProperty ClassProperty = DotvvmProperty.Register<string, BaseElement>((Expression<Func<BaseElement, object>>)(c => c.Class));
        public static readonly DotvvmProperty StyleProperty = DotvvmProperty.Register<string, BaseElement>((Expression<Func<BaseElement, object>>)(c => c.Style));
        public static readonly DotvvmProperty RoleProperty = DotvvmProperty.Register<string, BaseElement>((Expression<Func<BaseElement, object>>)(c => c.Role));

        public string Class
        {
            get => (string) GetValue(ClassProperty, true);
            set
            {
                var classProperty = ClassProperty;
                SetValue(classProperty, value ?? throw new ArgumentNullException(nameof (value)));
            }
        }
        
        public string Style
        {
            get => (string) GetValue(StyleProperty, true);
            set
            {
                var styleProperty = StyleProperty;
                SetValue(styleProperty, value ?? throw new ArgumentNullException(nameof (value)));
            }
        }
        
        public string Role
        {
            get => (string) GetValue(RoleProperty, true);
            set
            {
                var roleProperty = RoleProperty;
                SetValue(roleProperty, value ?? throw new ArgumentNullException(nameof (value)));
            }
        }

        public BaseElement(string? tagName) : base(tagName)
        {
        }
        
        protected override void AddAttributesToRender(IHtmlWriter writer, IDotvvmRequestContext context)
        {
            if (!string.IsNullOrEmpty(Class))
            {
                writer.AddAttribute("class", Class);
            }

            if (!string.IsNullOrEmpty(Style))
            {
                writer.AddAttribute("style", Style);
            }
            if (!string.IsNullOrEmpty(Role))
            {
                writer.AddAttribute("role", Role);
            }
            
            base.AddAttributesToRender(writer, context);
        }
        
    }
}