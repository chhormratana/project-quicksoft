using System;
using System.Linq.Expressions;
using DotVVM.Framework.Binding;
using DotVVM.Framework.Controls;
using DotVVM.Framework.Hosting;

namespace QuickSoft.MvcUI.HtmlElement
{
    public class HtmlContent : HtmlLiteral
    {
        public static readonly DotvvmProperty ContentProperty = DotvvmProperty.Register<string, HtmlContent>((Expression<Func<HtmlContent, object>>)(c => c.Content));
        
        public string[] Content
        {
            get => (string[]) GetValue(ContentProperty, true);
            set
            {
                var contentProperty = ContentProperty;
                SetValue(contentProperty, value ?? throw new ArgumentNullException(nameof (value)));
            }
        }
        public HtmlContent()
        {
            RenderWrapperTag = false;
        }

        protected override void RenderContents(IHtmlWriter writer, IDotvvmRequestContext context)
        {
            if (Content != null)
            {
                foreach (var c in Content)
                {
                    writer.WriteUnencodedText(c);
                }
            }
            base.RenderContents(writer, context);
        }
    }
}