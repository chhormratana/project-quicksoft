using System;
using System.Linq.Expressions;
using DotVVM.Framework.Binding;
using DotVVM.Framework.Controls;
using DotVVM.Framework.Hosting;

namespace QuickSoft.MvcUI.HtmlElement
{
    public class OrderList : BaseElement
    {
        public OrderList() : base("li")
        {
        }
    }
}