using DotVVM.Framework.Configuration;
using QuickSoft.MvcUI.Controls;
using QuickSoft.MvcUI.HtmlElement;

namespace QuickSoft.MvcUI
{
    public static class DependencyInjection
    {
        public static DotvvmConfiguration AddQuickSoftUi(this DotvvmConfiguration config)
        {
            // https://www.dotvvm.com/docs/3.0/pages/concepts/control-development/code-only-controls
            // https://www.dotvvm.com/docs/3.0/pages/concepts/localization-and-cultures/multi-language-applications?tabs=aspnetcore
            // register code-only controls and markup controls
            config.Markup.AddCodeControls("qs", typeof(Button));
            config.Markup.AddCodeControls("qs", typeof(NavigationDrawer));
            config.Markup.AddCodeControls("qs", typeof(AppBar));
            config.Markup.AddCodeControls("qs", typeof(Link));
            config.Markup.AddCodeControls("qs", typeof(LoadingButtonPostBackHandler));
            return config;
        }
    }
}