namespace QuickSoft.MvcUI.Controls
{
    public class GridView : DotVVM.Framework.Controls.GridView
    {
        
    }
    
    
}
/*
namespace DotVVM.Framework.Controls
{
  /// <summary>
  /// A multi-purpose grid control with advanced binding, templating options and sorting support.
  /// </summary>
  [ControlMarkupOptions(AllowContent = false, DefaultContentProperty = "Columns")]
  public class GridView : ItemsControl
  {
    private EmptyData? emptyDataContainer;
    private int numberOfRows;
    private HtmlGenericControl? head;
    public static readonly DotvvmProperty FilterPlacementProperty = DotvvmProperty.Register<GridViewFilterPlacement, GridView>((Expression<Func<GridView, object>>) (c => (object) c.FilterPlacement));
    public static readonly DotvvmProperty EmptyDataTemplateProperty = DotvvmProperty.Register<ITemplate, GridView>((Expression<Func<GridView, object>>) (t => t.EmptyDataTemplate));
    public static readonly DotvvmProperty ColumnsProperty = DotvvmProperty.Register<List<GridViewColumn>, GridView>((Expression<Func<GridView, object>>) (c => c.Columns));
    public static readonly DotvvmProperty RowDecoratorsProperty = DotvvmProperty.Register<List<Decorator>, GridView>((Expression<Func<GridView, object>>) (c => c.RowDecorators));
    public static readonly DotvvmProperty EditRowDecoratorsProperty = DotvvmProperty.Register<List<Decorator>, GridView>((Expression<Func<GridView, object>>) (c => c.EditRowDecorators));
    public static readonly DotvvmProperty SortChangedProperty = DotvvmProperty.Register<Action<string>, GridView>((Expression<Func<GridView, object>>) (c => c.SortChanged));
    public static readonly DotvvmProperty ShowHeaderWhenNoDataProperty = DotvvmProperty.Register<bool, GridView>((Expression<Func<GridView, object>>) (t => (object) t.ShowHeaderWhenNoData));
    public static readonly DotvvmProperty InlineEditingProperty = DotvvmProperty.Register<bool, GridView>((Expression<Func<GridView, object>>) (t => (object) t.InlineEditing));

    public GridView()
      : base("table")
    {
      this.SetValue(Internal.IsNamingContainerProperty, (object) true);
      this.Columns = new List<GridViewColumn>();
      this.RowDecorators = new List<Decorator>();
      if (!(this.GetType() == typeof (GridView)))
        return;
      this.LifecycleRequirements &= ~(ControlLifecycleRequirements.InvokeMissingInit | ControlLifecycleRequirements.InvokeMissingLoad);
    }

    /// <summary>
    /// Gets or sets the place where the filters will be created.
    /// </summary>
    public GridViewFilterPlacement FilterPlacement
    {
      get => (GridViewFilterPlacement) this.GetValue(GridView.FilterPlacementProperty, true);
      set => this.SetValue(GridView.FilterPlacementProperty, (object) value);
    }

    /// <summary>
    /// Gets or sets the template which will be displayed when the DataSource is empty.
    /// </summary>
    [MarkupOptions(MappingMode = MappingMode.InnerElement)]
    public ITemplate? EmptyDataTemplate
    {
      get => (ITemplate) this.GetValue(GridView.EmptyDataTemplateProperty, true);
      set => this.SetValue(GridView.EmptyDataTemplateProperty, (object) value);
    }

    /// <summary>
    /// Gets or sets a collection of columns that will be placed inside the grid.
    /// </summary>
    [MarkupOptions(AllowBinding = false, MappingMode = MappingMode.InnerElement)]
    [ControlPropertyBindingDataContextChange("DataSource", 0)]
    [CollectionElementDataContextChange(1)]
    public List<GridViewColumn>? Columns
    {
      get => (List<GridViewColumn>) this.GetValue(GridView.ColumnsProperty, true);
      set => this.SetValue(GridView.ColumnsProperty, (object) value);
    }

    /// <summary>
    /// Gets or sets a list of decorators that will be applied on each row which is not in the edit mode.
    /// </summary>
    [MarkupOptions(AllowBinding = false, MappingMode = MappingMode.InnerElement)]
    [ControlPropertyBindingDataContextChange("DataSource", 0)]
    [CollectionElementDataContextChange(1)]
    public List<Decorator>? RowDecorators
    {
      get => (List<Decorator>) this.GetValue(GridView.RowDecoratorsProperty, true);
      set => this.SetValue(GridView.RowDecoratorsProperty, (object) value);
    }

    /// <summary>
    /// Gets or sets a list of decorators that will be applied on each row in edit mode.
    /// </summary>
    [MarkupOptions(AllowBinding = false, MappingMode = MappingMode.InnerElement)]
    [ControlPropertyBindingDataContextChange("DataSource", 0)]
    [CollectionElementDataContextChange(1)]
    public List<Decorator>? EditRowDecorators
    {
      get => (List<Decorator>) this.GetValue(GridView.EditRowDecoratorsProperty, true);
      set => this.SetValue(GridView.EditRowDecoratorsProperty, (object) value);
    }

    /// <summary>
    /// Gets or sets the command that will be triggered when the user changed the sort order.
    /// </summary>
    [MarkupOptions(AllowHardCodedValue = false)]
    public Action<string?>? SortChanged
    {
      get => (Action<string>) this.GetValue(GridView.SortChangedProperty, true);
      set => this.SetValue(GridView.SortChangedProperty, (object) value);
    }

    /// <summary>
    /// Gets or sets whether the header row should be displayed when the grid is empty.
    /// </summary>
    [MarkupOptions(AllowBinding = false)]
    public bool ShowHeaderWhenNoData
    {
      get => (bool) this.GetValue(GridView.ShowHeaderWhenNoDataProperty, true);
      set => this.SetValue(GridView.ShowHeaderWhenNoDataProperty, (object) value);
    }

    /// <summary>
    /// Gets or sets whether the inline editing is allowed in the Grid. If so, you have to use a GridViewDataSet as the DataSource.
    /// </summary>
    [MarkupOptions(AllowBinding = false)]
    public bool InlineEditing
    {
      get => (bool) this.GetValue(GridView.InlineEditingProperty, true);
      set => this.SetValue(GridView.InlineEditingProperty, (object) value);
    }

    protected internal override void OnLoad(IDotvvmRequestContext context)
    {
      this.DataBind(context);
      base.OnLoad(context);
    }

    protected internal override void OnPreRender(IDotvvmRequestContext context)
    {
      this.DataBind(context);
      base.OnPreRender(context);
    }

    private void DataBind(IDotvvmRequestContext context)
    {
      this.Children.Clear();
      this.emptyDataContainer = (EmptyData) null;
      this.head = (HtmlGenericControl) null;
      IValueBinding dataSourceBinding = this.GetDataSourceBinding();
      object dataSource = this.DataSource;
      Action<string> sortCommand = (this.SortChanged ?? (typeof (ISortableGridViewDataSet).IsAssignableFrom(this.GetBinding(ItemsControl.DataSourceProperty) is IStaticValueBinding binding ? binding.ResultType : (Type) null) ? new Action<string>(this.SortChangedCommand) : (Action<string>) null)) ?? (Action<string>) (s =>
      {
        throw new DotvvmControlException((DotvvmBindableObject) this, "Cannot sort when DataSource is null.");
      });
      this.CreateHeaderRow(context, sortCommand);
      int num = 0;
      if (dataSource != null)
      {
        foreach (object obj in this.GetIEnumerableFromDataSource())
        {
          DataItemContainer placeholder = new DataItemContainer()
          {
            DataItemIndex = new int?(num)
          };
          placeholder.SetDataContextTypeFromDataSource((IBinding) dataSourceBinding);
          placeholder.DataContext = obj;
          placeholder.SetValue(Internal.PathFragmentProperty, (object) (this.GetPathFragmentExpression() + "/[" + num.ToString() + "]"));
          placeholder.ID = num.ToString();
          this.Children.Add((DotvvmControl) placeholder);
          this.CreateRowWithCells(context, placeholder);
          ++num;
        }
        this.numberOfRows = num;
      }
      else
        this.numberOfRows = 0;
      if (this.EmptyDataTemplate == null)
        return;
      this.emptyDataContainer = new EmptyData();
      this.emptyDataContainer.SetValue(HtmlGenericControl.VisibleProperty, this.GetValueRaw(HtmlGenericControl.VisibleProperty));
      this.emptyDataContainer.SetBinding(ItemsControl.DataSourceProperty, (IBinding) dataSourceBinding);
      this.EmptyDataTemplate.BuildContent(context, (DotvvmControl) this.emptyDataContainer);
      this.Children.Add((DotvvmControl) this.emptyDataContainer);
    }

    protected virtual void SortChangedCommand(string? expr)
    {
      object dataSource = this.DataSource;
      if (dataSource == null)
        throw new DotvvmControlException((DotvvmBindableObject) this, "Can not execute sort command, DataSource is null");
      ISortingOptions sortingOptions1 = dataSource is ISortableGridViewDataSet sortableGridViewDataSet ? sortableGridViewDataSet.SortingOptions : (ISortingOptions) null;
      if (sortingOptions1 == null)
        throw new DotvvmControlException((DotvvmBindableObject) this, "Can not execute sort command, DataSource does not have sorting options");
      if (sortingOptions1.SortExpression == expr)
      {
        ISortingOptions sortingOptions2 = sortingOptions1;
        sortingOptions2.SortDescending = !sortingOptions2.SortDescending;
      }
      else
      {
        sortingOptions1.SortExpression = expr;
        sortingOptions1.SortDescending = false;
      }
      if (!(dataSource is IPageableGridViewDataSet dataSet))
        return;
      dataSet.GoToFirstPage();
    }

    protected virtual void CreateHeaderRow(
      IDotvvmRequestContext context,
      Action<string?>? sortCommand)
    {
      this.head = new HtmlGenericControl("thead");
      this.Children.Add((DotvvmControl) this.head);
      IGridViewDataSet dataSource = this.DataSource as IGridViewDataSet;
      HtmlGenericControl htmlGenericControl1 = new HtmlGenericControl("tr");
      this.head.Children.Add((DotvvmControl) htmlGenericControl1);
      foreach (GridViewColumn column in this.Columns.NotNull<List<GridViewColumn>>("GridView.Columns must be set"))
      {
        HtmlGenericControl cell = new HtmlGenericControl("th");
        GridView.SetCellAttributes(column, cell, true);
        htmlGenericControl1.Children.Add((DotvvmControl) cell);
        column.CreateHeaderControls(context, this, sortCommand, cell, dataSource);
        if (this.FilterPlacement == GridViewFilterPlacement.HeaderRow)
          column.CreateFilterControls(context, this, cell, (ISortableGridViewDataSet) dataSource);
      }
      if (this.FilterPlacement != GridViewFilterPlacement.ExtraRow)
        return;
      HtmlGenericControl htmlGenericControl2 = new HtmlGenericControl("tr");
      this.head.Children.Add((DotvvmControl) htmlGenericControl2);
      foreach (GridViewColumn column in this.Columns.NotNull<List<GridViewColumn>>("GridView.Columns must be set"))
      {
        HtmlGenericControl cell = new HtmlGenericControl("th");
        GridView.SetCellAttributes(column, cell, true);
        htmlGenericControl2.Children.Add((DotvvmControl) cell);
        column.CreateFilterControls(context, this, cell, (ISortableGridViewDataSet) dataSource);
      }
    }

    private static void SetCellAttributes(
      GridViewColumn column,
      HtmlGenericControl cell,
      bool isHeaderCell)
    {
      if (!string.IsNullOrEmpty(column.Width))
        cell.Attributes["style"] = (object) ("width: " + column.Width);
      if (!isHeaderCell)
      {
        IValueBinding valueBinding = column.GetValueBinding(GridViewColumn.CssClassProperty);
        if (valueBinding != null)
        {
          cell.Attributes["class"] = (object) valueBinding;
        }
        else
        {
          if (string.IsNullOrWhiteSpace(column.CssClass))
            return;
          cell.Attributes["class"] = (object) column.CssClass;
        }
      }
      else
      {
        if (column.IsPropertySet(GridViewColumn.VisibleProperty))
          cell.SetValue(TableUtils.ColumnVisibleProperty, GridViewColumn.VisibleProperty.GetValue((DotvvmBindableObject) column));
        if (!column.IsPropertySet(GridViewColumn.HeaderCssClassProperty))
          return;
        cell.Attributes["class"] = column.GetValueRaw(GridViewColumn.HeaderCssClassProperty);
      }
    }

    private void CreateRowWithCells(IDotvvmRequestContext context, DataItemContainer placeholder)
    {
      bool isInEditMode = false;
      if (this.InlineEditing)
      {
        if (!(this.DataSource is IGridViewDataSet))
          throw new ArgumentException("You have to use GridViewDataSet with InlineEditing enabled.");
        isInEditMode = this.IsEditedRow(placeholder);
      }
      HtmlGenericControl row = this.CreateRow(placeholder, isInEditMode);
      foreach (GridViewColumn column in this.Columns.NotNull<List<GridViewColumn>>("GridView.Columns must be set"))
      {
        HtmlGenericControl cell = new HtmlGenericControl("td");
        cell.SetValue(Internal.DataContextTypeProperty, column.GetValueRaw(Internal.DataContextTypeProperty));
        GridView.SetCellAttributes(column, cell, false);
        row.Children.Add((DotvvmControl) cell);
        if (isInEditMode && column.IsEditable)
          column.CreateEditControls(context, (DotvvmControl) cell);
        else
          column.CreateControls(context, (DotvvmControl) cell);
      }
    }

    private HtmlGenericControl CreateRow(
      DataItemContainer placeholder,
      bool isInEditMode)
    {
      HtmlGenericControl htmlGenericControl = new HtmlGenericControl("tr");
      DotvvmControl dotvvmControl = (DotvvmControl) htmlGenericControl;
      List<Decorator> decoratorList = isInEditMode ? this.EditRowDecorators : this.RowDecorators;
      if (decoratorList != null)
      {
        foreach (Decorator decorator1 in decoratorList)
        {
          Decorator decorator2 = decorator1.Clone();
          decorator2.Children.Add(dotvvmControl);
          dotvvmControl = (DotvvmControl) decorator2;
        }
      }
      placeholder.Children.Add(dotvvmControl);
      return htmlGenericControl;
    }

    private PropertyInfo ResolvePrimaryKeyProperty()
    {
      IGridViewDataSet gridViewDataSet = (IGridViewDataSet) this.DataSource.NotNull<object>();
      string primaryKeyPropertyName = gridViewDataSet.RowEditOptions.PrimaryKeyPropertyName;
      if (string.IsNullOrEmpty(primaryKeyPropertyName))
        throw new DotvvmControlException((DotvvmBindableObject) this, "The IGridViewDataSet must specify the PrimaryKeyPropertyName property when inline editing is enabled on the GridView control!");
      Type enumerableType = gridViewDataSet.Items.GetType().GetEnumerableType();
      PropertyInfo property = enumerableType.GetProperty(primaryKeyPropertyName);
      return !(property == (PropertyInfo) null) ? property : throw new InvalidOperationException(string.Format("Type '{0}' does not contain a ", (object) enumerableType) + "'" + primaryKeyPropertyName + "' property.");
    }

    private bool IsEditedRow(DataItemContainer placeholder)
    {
      PropertyInfo propertyInfo = this.ResolvePrimaryKeyProperty();
      object obj = propertyInfo.GetValue(placeholder.DataContext);
      if (obj != null)
      {
        object editRowId = ((IRowEditGridViewDataSet) this.DataSource).RowEditOptions.EditRowId;
        if (editRowId != null && obj.Equals(ReflectionUtils.ConvertValue(editRowId, propertyInfo.PropertyType)))
          return true;
      }
      return false;
    }

    private void CreateTemplates(
      IDotvvmRequestContext context,
      DataItemContainer placeholder,
      bool isInEditMode = false)
    {
      HtmlGenericControl row = this.CreateRow(placeholder, isInEditMode);
      foreach (GridViewColumn column in this.Columns.NotNull<List<GridViewColumn>>("GridView.Columns must be set"))
      {
        HtmlGenericControl cell = new HtmlGenericControl("td");
        cell.SetValue(Internal.DataContextTypeProperty, column.GetValueRaw(Internal.DataContextTypeProperty));
        row.Children.Add((DotvvmControl) cell);
        GridView.SetCellAttributes(column, cell, false);
        if (isInEditMode && column.IsEditable)
          column.CreateEditControls(context, (DotvvmControl) cell);
        else
          column.CreateControls(context, (DotvvmControl) cell);
      }
    }

    protected override void RenderContents(IHtmlWriter writer, IDotvvmRequestContext context)
    {
      this.head?.Render(writer, context);
      string bindingExpression = this.GetForeachDataBindExpression().GetKnockoutBindingExpression((DotvvmBindableObject) this);
      if (this.RenderOnServer)
        writer.AddKnockoutDataBind("dotvvm-SSR-foreach", "{data:" + bindingExpression + "}");
      else
        writer.AddKnockoutForeachDataBind(bindingExpression);
      writer.RenderBeginTag("tbody");
      if (this.RenderOnServer)
      {
        int num = 0;
        foreach (DotvvmControl dotvvmControl in this.Children.Except<DotvvmControl>((IEnumerable<DotvvmControl>) new HtmlGenericControl[2]
        {
          this.head,
          (HtmlGenericControl) this.emptyDataContainer
        }))
        {
          dotvvmControl.Render(writer, context);
          ++num;
        }
      }
      else if (this.InlineEditing)
      {
        DataItemContainer dataItemContainer1 = new DataItemContainer();
        dataItemContainer1.DataContext = (object) null;
        DataItemContainer placeholder1 = dataItemContainer1;
        placeholder1.SetDataContextTypeFromDataSource(this.GetBinding(ItemsControl.DataSourceProperty).NotNull<IBinding>());
        placeholder1.SetValue(Internal.PathFragmentProperty, (object) (this.GetPathFragmentExpression() + "/[$index]"));
        placeholder1.SetValue(Internal.ClientIDFragmentProperty, this.GetValueRaw(Internal.CurrentIndexBindingProperty));
        writer.WriteKnockoutDataBindComment("if", "!$gridViewDataSetHelper.isInEditMode($context)");
        this.CreateTemplates(context, placeholder1);
        this.Children.Add((DotvvmControl) placeholder1);
        placeholder1.Render(writer, context);
        writer.WriteKnockoutDataBindEndComment();
        DataItemContainer dataItemContainer2 = new DataItemContainer();
        dataItemContainer2.DataContext = (object) null;
        DataItemContainer placeholder2 = dataItemContainer2;
        placeholder2.SetDataContextTypeFromDataSource(this.GetBinding(ItemsControl.DataSourceProperty).NotNull<IBinding>());
        placeholder2.SetValue(Internal.PathFragmentProperty, (object) (this.GetPathFragmentExpression() + "/[$index]"));
        placeholder2.SetValue(Internal.ClientIDFragmentProperty, this.GetValueRaw(Internal.CurrentIndexBindingProperty));
        writer.WriteKnockoutDataBindComment("if", "$gridViewDataSetHelper.isInEditMode($context)");
        this.CreateTemplates(context, placeholder2, true);
        this.Children.Add((DotvvmControl) placeholder2);
        placeholder2.Render(writer, context);
        writer.WriteKnockoutDataBindEndComment();
      }
      else
      {
        DataItemContainer dataItemContainer = new DataItemContainer();
        dataItemContainer.DataContext = (object) null;
        DataItemContainer placeholder = dataItemContainer;
        placeholder.SetDataContextTypeFromDataSource(this.GetBinding(ItemsControl.DataSourceProperty).NotNull<IBinding>());
        placeholder.SetValue(Internal.PathFragmentProperty, (object) (this.GetPathFragmentExpression() + "/[$index]"));
        placeholder.SetValue(Internal.ClientIDFragmentProperty, this.GetValueRaw(Internal.CurrentIndexBindingProperty));
        this.Children.Add((DotvvmControl) placeholder);
        this.CreateRowWithCells(context, placeholder);
        placeholder.Render(writer, context);
      }
      writer.RenderEndTag();
    }

    protected override void RenderBeginTag(IHtmlWriter writer, IDotvvmRequestContext context)
    {
      if (!this.ShowHeaderWhenNoData)
        writer.WriteKnockoutDataBindComment("if", this.GetForeachDataBindExpression().GetProperty<DataSourceLengthBinding>().Binding.CastTo<IValueBinding>().GetKnockoutBindingExpression((DotvvmBindableObject) this));
      base.RenderBeginTag(writer, context);
    }

    protected override void RenderControl(IHtmlWriter writer, IDotvvmRequestContext context)
    {
      if (this.RenderOnServer && this.numberOfRows == 0 && !this.ShowHeaderWhenNoData)
        this.emptyDataContainer?.Render(writer, context);
      else
        base.RenderControl(writer, context);
    }

    protected override void RenderEndTag(IHtmlWriter writer, IDotvvmRequestContext context)
    {
      base.RenderEndTag(writer, context);
      if (!this.ShowHeaderWhenNoData)
        writer.WriteKnockoutDataBindEndComment();
      this.emptyDataContainer?.Render(writer, context);
    }

    protected override void AddAttributesToRender(IHtmlWriter writer, IDotvvmRequestContext context)
    {
      Type enumerableType = this.GetDataSourceBinding().ResultType.GetEnumerableType();
      string str = JsonConvert.SerializeObject((object) context.Services.GetRequiredService<UserColumnMappingCache>().GetMapping(enumerableType));
      writer.AddKnockoutDataBind("dotvvm-gridviewdataset", "{'mapping':" + str + ",'dataSet':" + this.GetDataSourceBinding().GetKnockoutBindingExpression((DotvvmBindableObject) this, true) + "}");
      base.AddAttributesToRender(writer, context);
    }

    public override IEnumerable<DotvvmBindableObject> GetLogicalChildren() => base.GetLogicalChildren().Concat<DotvvmBindableObject>((IEnumerable<DotvvmBindableObject>) this.Columns).Concat<DotvvmBindableObject>((IEnumerable<DotvvmBindableObject>) this.RowDecorators);
  }
}

*/