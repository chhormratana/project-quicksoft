namespace QuickSoft.MvcUI.Controls
{
    public class AppBarItem
    {
        public string Url { get; set; }
        public string Text { get; set; }
    }
}