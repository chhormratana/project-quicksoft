using DotVVM.Framework.Controls;
using DotVVM.Framework.Hosting;
using DotVVM.Framework.Binding;
using System.Linq.Expressions;
using System;
using QuickSoft.MvcUI.HtmlElement;

namespace QuickSoft.MvcUI.Controls
{
    public class AppBar : HtmlGenericControl
    {
        public static readonly DotvvmProperty ItemProperty = DotvvmProperty.Register<AppBarItem, AppBar>((Expression<Func<AppBar, object>>)(c => c.Item));
        public AppBarItem Item
        {
            get => (AppBarItem) GetValue(ItemProperty, true);
            set => SetValue(ItemProperty, value ?? throw new ArgumentNullException(nameof (value)));
        }

        public AppBar() : base("div")
        {
        }

        protected override void AddAttributesToRender(IHtmlWriter writer, IDotvvmRequestContext context)
        {
            writer.AddAttribute("class", "row border-bottom");
            base.AddAttributesToRender(writer, context);
        }

        protected override void RenderContents(IHtmlWriter writer, IDotvvmRequestContext context)
        {
            var nav = new Nav
            {
                Class = "navbar navbar-static-top",
                Role = "navigation",
                Style = "margin-bottom: 0",
                Children =
                {
                    new Div
                    {
                        Class = "navbar-header",
                        Children =
                        {
                            new Link
                            {
                                Class = "navbar-minimalize minimalize-styl-2 btn btn-success",
                                Href = "#",
                                Children = {new Icon {Class = "fa fa-bars"}}
                            }
                        }
                    },
                    new UnOrderList
                    {
                        Class = "nav navbar-top-links navbar-right",
                        Children =
                        {
                            new OrderList
                            {
                                Children =
                                {
                                    new Link
                                    {
                                        Href = Item.Url,
                                        Children =
                                        {
                                            new Icon {Class = "fa fa-sign-out"},
                                            new HtmlGenericControl("span") {InnerText = Item.Text},
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            };
            Children.Add(nav);
            base.RenderContents(writer, context);
        }
    }
}