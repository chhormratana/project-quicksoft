using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using DotVVM.Framework.Binding;
using DotVVM.Framework.Controls;

namespace QuickSoft.MvcUI.Controls
{
    public class LoadingButtonPostBackHandler : PostBackHandler
    {
        public static readonly DotvvmProperty ButtonIdProperty = DotvvmProperty.Register<string, LoadingButtonPostBackHandler>((Expression<Func<LoadingButtonPostBackHandler, object>>) (c => c.ButtonId));
        public static readonly DotvvmProperty JsHandlerProperty = DotvvmProperty.Register<string, LoadingButtonPostBackHandler>((Expression<Func<LoadingButtonPostBackHandler, object>>) (c => c.JsHandler));
        [MarkupOptions(AllowHardCodedValue = true, Required = true)]
        public string ButtonId
        {
            get => (string) GetValue(ButtonIdProperty);
            set => SetValue(ButtonIdProperty, value ?? throw new ArgumentNullException(nameof (value)));
        }
        
        [MarkupOptions(AllowHardCodedValue = true, Required = true)]
        public string JsHandler
        {
            get => (string) GetValue(JsHandlerProperty);
            set => SetValue(JsHandlerProperty, value ?? throw new ArgumentNullException(nameof (value)));
        }
        
        protected override Dictionary<string, object> GetHandlerOptions()
        {
            return new Dictionary<string, object>
            {
                ["buttonId"] = GetValueRaw(ButtonIdProperty),
                ["JsHandler"] = GetValueRaw(JsHandlerProperty)
            };
        }

        protected override string ClientHandlerName => "LoadingButtonPostBackHandler";
    }
}