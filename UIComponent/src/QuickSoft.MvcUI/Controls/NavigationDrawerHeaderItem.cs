namespace QuickSoft.MvcUI.Controls
{
    public class NavigationDrawerHeaderItem
    {
        public string RoundLogoUrl { get; set; }
        public string LogoUrl { get; set; }

        public string LogoSize { get; set; } = "180"; // use as default
        public string RoundLogoSize { get; set; } = "30"; // use as default

        public string AltLogo { get; set; } = "ImageLogo";
        public string AltRoundLogo { get; set; } = "ImageLogo";
    }
}