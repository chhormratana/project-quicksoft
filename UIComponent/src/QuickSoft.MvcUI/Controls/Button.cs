using System;
using System.Linq.Expressions;
using DotVVM.Framework.Binding;
using DotVVM.Framework.Controls;
using DotVVM.Framework.Hosting;

namespace QuickSoft.MvcUI.Controls
{
    public class Button : DotVVM.Framework.Controls.Button
    {
        public static readonly DotvvmProperty ClassProperty = DotvvmProperty.Register<string, Button>((Expression<Func<Button, object>>)(c => c.Class));

        public string Class
        {
            get => (string) GetValue(ClassProperty);
            set => SetValue(ClassProperty, value ?? throw new ArgumentNullException(nameof (value)));
        }

        public Button()
        {
            Class = "btn btn-primary";
            ButtonTagName = ButtonTagName.button;
        }
        
        protected override void AddAttributesToRender(IHtmlWriter writer, IDotvvmRequestContext context)
        {
            if (!string.IsNullOrEmpty(Class))
            {
                writer.AddAttribute("class", Class);
            }
            
            base.AddAttributesToRender(writer, context);
        }
    }
}