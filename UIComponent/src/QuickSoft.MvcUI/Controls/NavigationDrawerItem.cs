using System.Collections.Generic;

namespace QuickSoft.MvcUI.Controls
{
    public class NavigationDrawerItem
    {
        public string Url { get; set; }
        public string IconClass { get; set; }
        public string Text { get; set; }
        public string IsActive { get; set; }
        public string SmallLabel { get; set; }
        public List<NavigationDrawerItem> Children { get; set; }
    }
}