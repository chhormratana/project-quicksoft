using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DotVVM.Framework.Binding;
using DotVVM.Framework.Controls;
using DotVVM.Framework.Hosting;
using QuickSoft.MvcUI.HtmlElement;

namespace QuickSoft.MvcUI.Controls
{
    public class NavigationDrawer : Nav
    {
        public static readonly DotvvmProperty HeaderProperty = DotvvmProperty.Register<NavigationDrawerHeaderItem, NavigationDrawer>((Expression<Func<NavigationDrawer, object>>) (c => c.Header));
        public static readonly DotvvmProperty ContentProperty = DotvvmProperty.Register<List<NavigationDrawerItem>, NavigationDrawer>((Expression<Func<NavigationDrawer, object>>)(c => c.Content));

        public NavigationDrawerHeaderItem Header
        {
            get => (NavigationDrawerHeaderItem) GetValue(HeaderProperty, true);
            set
            {
                var headerProperty = HeaderProperty;
                SetValue(headerProperty, value ?? throw new ArgumentNullException(nameof (value)));
            }
        }

        public List<NavigationDrawerItem> Content
        {
            get => (List<NavigationDrawerItem>)GetValue(ContentProperty, true);
            set
            {
                var contentProperty = ContentProperty;
                SetValue(contentProperty, value ?? throw new ArgumentNullException(nameof(value)));
            }
        }


        public NavigationDrawer()
        {
            Class = "navbar-default navbar-static-side";
            Role = "navigation";
        }

        protected override void RenderContents(IHtmlWriter writer, IDotvvmRequestContext context)
        {
            var sidebar = new Div
            {
                Class = "sidebar-collapse"
            };

            var ul = new UnOrderList
            {
                Class = "nav metismenu",
                ID = "side-menu"
            };
            
            if (Header != null)
            {
                ul.Children.Add(CreateHeader());
            }

            
            if (Content != null)
            {
                foreach (var children in CreateContent(Content))
                {
                    ul.Children.Add(children);
                }
            }

            sidebar.Children.Add(ul);
            Children.Add(sidebar);
            base.RenderContents(writer, context);
        }

        private static IEnumerable<HtmlGenericControl> CreateContent(IEnumerable<NavigationDrawerItem> list, bool containChild = false)
        {
            var result = new List<HtmlGenericControl>();
            foreach (var drawerItem in list)
            {
                var li = new OrderList();
                var a = new Link { Href = drawerItem.Url ?? "#" };

                if (!string.IsNullOrEmpty(drawerItem.IsActive))
                {
                    li.Attributes["class"] = drawerItem.IsActive;
                }

                

                #region Icon and small label
                if (drawerItem.IconClass != null)
                {
                    var icon = new Icon { Class =  drawerItem.IconClass };
                    a.Children.Add(icon);
                }

                if (!containChild)
                {
                    var spanText = new Span
                    {
                        Class = "nav-label",
                        Text = drawerItem.Text
                    };
                    a.Children.Add(spanText);
                }
                else
                {
                    var content = new HtmlContent { Html = drawerItem.Text };
                    a.Children.Add(content);
                }

                // If you want to add small label
                if (drawerItem.SmallLabel != null)
                {
                    var smallLabel = new Span
                    {
                        Class = "label label-primary float-right", 
                        Text = drawerItem.SmallLabel
                    };
                    a.Children.Add(smallLabel);
                }
                
                li.Children.Add(a);
                #endregion
                

                // Sub menu
                if (drawerItem.Children != null)
                {
                    var arrow = new Span { Class = "fa arrow"};
                    a.Children.Add(arrow);
                    
                    var subMenu = new UnOrderList
                    {
                        Class = "nav nav-second-level collapse " + (!string.IsNullOrEmpty(drawerItem.IsActive)? "in" : "")
                    };

                    foreach (var children in CreateContent(drawerItem.Children, true))
                    {
                        subMenu.Children.Add(children);
                    }
                    li.Children.Add(subMenu);
                }
                result.Add(li);
            }
            return result;
        }
        
        private HtmlGenericControl CreateHeader()
        {
            var a = new Link { Href = "/" };
            var liHeader = new OrderList {Class = "nav-header"};

            var profileElement = new Div { Class = "profile-element"};

            var logo = new Image
            {
                Alt = Header.AltLogo,
                Width = Header.LogoSize,
                Src = Header.LogoUrl
            };
            profileElement.Children.Add(logo);

            var logoElement = new Div { Class = "logo-element"};
            
            var roundLogo = new Image
            {
                Alt = Header.AltRoundLogo,
                Width = Header.RoundLogoSize,
                Src = Header.RoundLogoUrl
            };
            
            logoElement.Children.Add(roundLogo);
            a.Children.Add(profileElement);
            a.Children.Add(logoElement);
            var span = new Span { Children = { a }};
            liHeader.Children.Add(span);
            return liHeader;
        }
    }
}