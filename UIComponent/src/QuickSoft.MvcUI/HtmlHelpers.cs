using DotVVM.Framework.Routing;
using System;
using System.Linq;
using System.Collections.Generic;

namespace QuickSoft.MvcUI
{
    public static class HtmlHelpers
    {
        public static string IsSelected(this RouteBase routeBase, string routeName, string cssClass = null)
        {
            if (string.IsNullOrEmpty(cssClass))
                cssClass = "active";

            var currentName = routeBase?.RouteName;

            return currentName == routeName ? cssClass : string.Empty;
        }

        public static string IsSelected(this RouteBase routeBase, string [] routeNames, string cssClass = null)
        {

            var list = new List<string>();
            foreach(var routeName in routeNames) {
                list.Add(IsSelected(routeBase, routeName, cssClass));
            }

            return list.FirstOrDefault(active => !string.IsNullOrEmpty(active));
        }
    }
}